/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author juanma
 */
public class Lectura {
    
    protected BufferedReader buffer() {
        
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        
        return buffer;
    }
    
    public String string() {
        
        String lectura = "";
        
        try {
            lectura = this.buffer().readLine();
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return lectura;
    }
    
    public int integer() {
        
        int lectura = 0;
        
        try {
            lectura = Integer.parseInt(this.buffer().readLine());
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return lectura;
    }
    
}
