/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;


/**
 *
 * @author juanma
 */
public class Escritura {
    
    public void mostrarMenu(String[] options) {
        
        for(int i = 0; i < options.length; i++) {
            System.out.println(i + ". " + options[i]);
        }
        
        System.out.print("Opcion? ");
    }
    
    public void mostrarError(String error) {
        
        System.out.println("Error: " + error);
    }
    
    public void mostrar(String string) {
        
        System.out.println(string);
    }
    
    public void mostrarOpcion(String string) {
        
        System.out.print(string);
    }

}
