/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio0319;

import Modelo.Animal;
import Modelo.Persona;
import Modelo.Ser;
import Vista.Interface;

/**
 *
 * @author juanma
 */
public class Ejercicio0319 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int opcion = -1, edad = -1;
        Ser ser = null;
        String[] datos = new String[2];
        
        Interface vista = new Interface();
        
        while(opcion != 0) {
            
            opcion = vista.elegirOpcion();
           
            switch(opcion) {
                case 0:
                    vista.mostrarFin();
                    break;
                case 1:
                    ser = new Persona();
                    tomarDatos(ser);
                    vista.mostrarDatos(ser);
                    break;
                case 2:
                    ser = new Animal();
                    tomarDatos(ser);
                    vista.mostrarDatos(ser);
                    break;
                default:
                    vista.mostrarOpcionIncorrecta();
                    break;
            }
        }
    }
    
    private static void tomarDatos(Ser ser) {
        
        int edad = 0;
        Interface vista = new Interface();
        
        vista.empezarTomaDatos(ser);
        ser.setNombre(vista.pedirNombre());
        edad = vista.pedirEdad();
        while(edad < 0) {
            vista.mostrarError("Valor debe ser positivo en la edad");
            edad = vista.pedirEdad();
        }
        ser.setEdad(edad);
    }
    
}
