/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Ser;
import Util.Escritura;
import Util.Lectura;

/**
 *
 * @author juanma
 */
public class Interface {
    
    protected Escritura escritura;
    protected Lectura lectura;
    
    public Interface() {
        
        this.escritura = new Escritura();
        this.lectura = new Lectura();
        
    }
    
    public int elegirOpcion() {
        
        int opcion = -1;
        
        String[] opciones = {"Fin", "Persona", "Animal"};
        this.escritura.mostrarMenu(opciones);

        opcion = this.lectura.integer();
        
        return opcion;
    }
    
    public void empezarTomaDatos(Ser ser) {
        
        this.escritura.mostrar("TOMA DE DATOS " + ser.getClass().getSimpleName().toUpperCase());
        
    }
    
    public String pedirNombre() {
        
        this.escritura.mostrarOpcion("Nombre? ");
        
        return this.lectura.string();
    }
    
    public int pedirEdad() {
        
        this.escritura.mostrarOpcion("Edad? ");
        
        return this.lectura.integer();
    }
    
    public void mostrarFin() {
        
        this.escritura.mostrar("FIN");
    }
    
    public void mostrarOpcionIncorrecta() {
        
        this.escritura.mostrarError("Opcion incorrecta");
    }
    
    public void mostrarError(String error) {
        
        this.escritura.mostrarError(error);
    }
    
    public void mostrarDatos(Ser ser) {
        
        this.escritura.mostrar("MOSTRANDO DATOS " + ser.getClass().getSimpleName().toUpperCase());
        this.escritura.mostrar(ser.toString());
    }
}
